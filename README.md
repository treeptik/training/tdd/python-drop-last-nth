# TDD Python Drop Last `n`th

[![(c) Treeptik, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Test Driven Development of dropping `n`th line from the end in Python.

## License

See [License](LICENSE.md).

## Problem

Develop a command line program that given a single integer argument `n` writes
all lines from standard input to standard output except for the `n`th line from
the end.

## Running Tests

```shell
pipenv install --dev
pipenv run pytest
```

## Running Program

```shell
cat very-large-file | pipenv run python src/drop_last_nth.py 5
```
