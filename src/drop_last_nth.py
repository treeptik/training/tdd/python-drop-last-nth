import sys
import argparse


def drop_last_nth(input, n):
    buffer = []
    for line in input:
        if len(buffer) <= n:
            buffer.append(line)
        else:
            yield buffer.pop(0)
            buffer.append(line)
    buffer.pop(0)
    for line in buffer:
        yield line


def main():
    parser = argparse.ArgumentParser("drop_last_nth")
    parser.add_argument('n', type=int)
    args = parser.parse_args()

    input = sys.stdin
    n = args.n

    for line in drop_last_nth(input, n):
        print(line, end="")


if __name__ == "__main__":
    main()
