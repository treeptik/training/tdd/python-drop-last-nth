import pytest

from drop_last_nth import drop_last_nth


class TestGivenSingleton:
    @pytest.fixture()
    def input(self):
        return ["this line must be removed"]

    def test_when_remove_only_line_then_empty_output(self, input):
        output = list(drop_last_nth(input, 0))

        assert len(output) == 0


class TestGivenLargerFile:
    @pytest.fixture()
    def input(self):
        return [
            "this line should be kept",
            "this line should be kept",
            "this line should be kept",
            "this line should be kept",
            "this line must be removed"
            ]

    class TestWhenRemoveLastLine:
        @pytest.fixture()
        def when(self, input):
            return list(drop_last_nth(input, 0))

        def test_then_last_line_not_there(self, when):
            assert len(when) == 4
            assert "this line should be kept" in when
            assert "this line must be removed" not in when

    class TestWhenRemoveFirstLine:
        @pytest.fixture()
        def when(self, input):
            return list(drop_last_nth(input, 4))

        def test_then_first_line_not_there(self, when):
            assert len(when) == 4
            assert "this line must be removed" in when
